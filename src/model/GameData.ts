import Card from "./Card"

let cardList:Array<Card> = []
for (let i = 1; i <= 9; i++) {
    cardList.push({
        name: `A${i}`,
        svg: `zip/cards/A${i}.svg`,
        fgSnaps: ['A1', 'A2', 'A3', 'A4', 'A5', 'A6', 'A7', 'A8', 'A9']
    } as Card)
}
for (let i = 1; i <= 8; i++) {
    cardList.push({
        name: `B${i}`,
        svg: `zip/cards/B${i}.svg`,
        fgSnaps: ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7', 'B8']
    } as Card)
}
for (let i = 1; i <= 7; i++) {
    cardList.push({
        name: `C${i}`,
        svg: `zip/cards/C${i}.svg`,
        fgSnaps: ['C1', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7']
    } as Card)
}
for (let i = 1; i <= 8; i++) {
    cardList.push({
        name: `D${i}`,
        svg: `zip/cards/D${i}.svg`,
        fgSnaps: ['D1', 'D2', 'D3', 'D4', 'D5', 'D6', 'D7', 'D8']
    } as Card)
}
for (let i = 1; i <= 7; i++) {
    cardList.push({
        name: `E${i}`,
        svg: `zip/cards/E${i}.svg`,
        fgSnaps: ['E1', 'E2', 'E3', 'E4', 'E5', 'E6', 'E7']
    } as Card)
}
for (let i = 1; i <= 9; i++) {
    cardList.push({
        name: `F${i}`,
        svg: `zip/cards/F${i}.svg`,
        fgSnaps: ['F1', 'F2', 'F3', 'F4', 'F5', 'F6', 'F7', 'F8', 'F9']
    } as Card)
}
for (let i = 1; i <= 6; i++) {
    cardList.push({
        name: `G${i}`,
        svg: `zip/cards/G${i}.svg`,
        fgSnaps: [ 'G' + (i + 6) ]
    } as Card)
}
for (let i = 1; i <= 6; i++) {
    cardList.push({
        name: `H${i}`,
        svg: `zip/cards/H${i}.svg`,
        fgSnaps: [ 'H1', 'H2', 'H3', 'H4', 'H5', 'H6 ']
    } as Card)
}
for (let i = 1; i <= 6; i++) {
    cardList.push({
        name: `J${i}`,
        svg: `zip/cards/J${i}.svg`,
        fgSnaps: [ 'J1', 'J2', 'J3', 'J4', 'J5', 'J6 ']
    } as Card)
}




class Message {

}


export default class GameData {
    
    code: string = "123abc"

    names: Array<string> = ["You", "CPU"]

    score: Array<number> = [0, 0]

    deck: Array<Card> = []

    myFlop: Array<Card | undefined> = []

    oppFlop: Array<Card | undefined> = []

    left?: Card

    right?: Card

    myTurn: boolean = true

    messages: Array<Message> = []

    snap: boolean = false

    constructor() {
        this.deck = cardList.slice(0)

        for (let i = 0; i < 5; i++) {
            this.draw(i, this.myFlop)            
        }

        for (let i = 0; i < 5; i++) {
            this.draw(i, this.oppFlop)            
        }
    }
      

    draw(to:number, dest:Array<Card | undefined>) {
        let idx = Math.floor(Math.random() * this.deck.length)
        let c = this.deck.splice(idx, 1)[0]

        dest.splice(to, 1, c)        
    }

    play(i:number, from:Array<Card | undefined>) {
        console.log("ping")

        if(this.myFlop[i]) {
            this.right = this.myFlop[i]  
            this.myFlop.splice(i, 1, undefined)          

            let self = this
            setTimeout(() => {
                if (self.deck.length > 0) {
                    self.draw(i, this.myFlop)
                }
            }, 200)                    
        }
    }


    playerPlay(i:number) {
        if (this.myTurn) {
            this.play(i, this.myFlop)

            let self = this
            if (this.left && this.left.fgSnaps.filter((n) => self.right && n == self.right.name).length > 0) {

                if (this.myTurn) {
                    let s = this.score[0]
                    this.score.splice(0, 1, s+1)
                } else {
                    let s = this.score[1]
                    this.score.splice(1, 1, s+1)
                }

                self.snap = true
                setTimeout(() => {
                    self.snap = false
                }, 200)         
            }

            this.myTurn = false

            
            setTimeout(() => {
                this.opponentPlay()
            }, 1000)         
        }
    }

    opponentPlay() {
        let validChoices = [0, 1, 2, 3, 4].filter((i) => this.oppFlop[i])
        let num = Math.floor(Math.random() * validChoices.length)

        this.left = this.oppFlop[num]  
        this.oppFlop.splice(num, 1, undefined)          

        let self = this
        setTimeout(() => {
            if (self.deck.length > 0) {
                self.draw(num, this.oppFlop)
                self.myTurn = true
            }
        }, 200)                    
    }

}
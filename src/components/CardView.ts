import Vue from 'vue'
import Component from 'vue-class-component'

import Card from "../model/Card"

@Component({
    props: {
        card: Object as () => Card,
        opp: Boolean,
        back: Boolean 
    },
    template: `
      <div>
        <div v-if="!this.card || this.back" v-bind:class="{ 'card-view': true, empty: !this.card, opp: this.opp, back: this.back && this.card }" ></div>
        <div v-if="this.card && !this.back" v-bind:class="{ 'card-view': true, opp: this.opp }" v-on:click="$emit('card-click')" ><img :src="this.card.svg" /></div>
      </div>
    `
})
export default class CardView extends Vue {

  opp!: boolean

  back!: boolean
    
}
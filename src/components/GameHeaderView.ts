import Vue from 'vue'
import Component from 'vue-class-component'
import GameData from '../model/GameData';

@Component({
    props: {
        gameData: GameData
    },
    template: `
        <div class="game-header">        
        Game code: {{ gameData.code }}
      </div>
    `
})
export default class GameHeaderView extends Vue {
    
    
}
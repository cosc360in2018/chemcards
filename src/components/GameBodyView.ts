import Vue from 'vue'
import { Component } from 'vue-property-decorator'
import GameData from '../model/GameData';

import CardView from "./CardView"
import StackView from "./StackView"

@Component({
    props: {
        gameData: GameData
    },
    template: `
        <div class="game-header">
          <div class="flop-view opp">
            <card-view v-for="(item, index) in gameData.oppFlop" :card="item" opp="true" back="true">123</card-view>
          </div>
          <div class="playing-area">
            <stack-view :stack="this.gameData.deck"></stack-view> 
            <div v-bind:class="{ 'play-wrap': true, 'snap': this.gameData.snap }">       
                <card-view opp="true" :card="this.gameData.left"></card-view>
                <card-view :card="this.gameData.right"></card-view>
            </div>

            <div class="scores">
            <div class="score left">
                <span v-bind:class="{ turn: this.gameData.myTurn }">{{ this.gameData.score[0] }}</span>
                <span v-bind:class="{ turn: this.gameData.myTurn }">{{ this.gameData.names[0] }}</span>
            </div>
            <div class="score right">
                <span v-bind:class="{ turn: !this.gameData.myTurn }">{{ this.gameData.score[1] }}</span>
                <span v-bind:class="{ turn: !this.gameData.myTurn }">{{ this.gameData.names[1] }}</span>
            </div>
            <div class="subtitle" v-if=" this.gameData.myTurn" >{{ this.gameData.names[0] }} to play</div>
            <div class="subtitle" v-if=" !this.gameData.myTurn" >{{ this.gameData.names[1] }} to play</div>  
          </div>
          </div>
          <div class="flop-view">
            <card-view v-for="(item, index) in gameData.myFlop" :card="item" @card-click="play(index)" >123</card-view>
          </div>
        </div>
    `,
    components: {
        CardView,
        StackView
    }
})
export default class GameBodyView extends Vue {

    gameData!: GameData

    play(i:number) {
        this.gameData.playerPlay(i) 
    }
    
    
}
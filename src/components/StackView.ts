import Vue from 'vue'
import Component from 'vue-class-component'

import Card from "../model/Card"

@Component({
    props: {
        stack: Array as () => Array<Card>
    },
    template: `
      <div class="stack-view">
        <span>{{ stack.length }} cards left</span>
      </div>
    `
})
export default class StackView extends Vue {
    
}
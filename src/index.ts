// src/index.ts

import Vue from "vue";
import GameHeaderView from "./components/GameHeaderView";
import GameData from "./model/GameData";
import GameBodyView from "./components/GameBodyView";
import CardView from "./components/CardView"

let gd = new GameData()

let v = new Vue({
    el: "#app",
    template: `
    <div class="card-table">
        <game-header-view :game-data="gd"></game-header-view>
        <game-body-view :game-data="gd"></game-body-view>
    </div>`,
    data: {
        gd: gd
    },
    components: {
        GameHeaderView,
        GameBodyView,
        CardView
    }
});